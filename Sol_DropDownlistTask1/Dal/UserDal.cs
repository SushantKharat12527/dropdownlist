﻿using Sol_DropDownlistTask1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_DropDownlistTask1.Dal
{
    public class UserDal
    {
        #region Public Methods
        public async Task<IEnumerable<CityEntity>> GetCitiesAsync()
        {
            return await Task.Run(() => {

                return
                    new
                        List<CityEntity>()
                    {
                         new CityEntity()
                         {
                             CityId=1,
                             CityName="Mumbai"
                         },

                         new CityEntity()
                         {
                             CityId=2,
                             CityName="Thane"
                         },

                          new CityEntity()
                         {
                             CityId=3,
                             CityName="Pune"
                         },

                          new CityEntity()
                         {
                             CityId=3,
                             CityName="Nashik"
                         },

                    };


            });
        }
        #endregion 
    }
}
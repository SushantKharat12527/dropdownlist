﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="DynamicDDL.aspx.cs" Inherits="Sol_DropDownlistTask1.DynamicDDL" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:ScriptManager ID="scriptManger" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                
                    <table>
                        <tr>
                            <td>
                                   <asp:DropDownList ID="ddlCity" runat="server" DataTextField="CityName" DataValueField="CityId"></asp:DropDownList>  
                             </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                            </td>
                        </tr>

                    </table>
                    
                   
                
                
                </ContentTemplate>
            </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>

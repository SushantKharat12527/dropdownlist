﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_DropDownlistTask1.Models
{
    public class CityEntity
    {
        public decimal CityId { get; set; }

        public String CityName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_DropDownlistTask1
{
    public partial class Task2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
        protected void ddl_SelectedIndexChangeColor(object sender, EventArgs e)
        {
            // Get Selected Index from DropDown List
            //lblStatus.Text = ddlColors.SelectedIndex.ToString();

            // Get Selected Text from DropDown List
           var Value= ddlColor.SelectedItem.Value.ToString();

            if (Value == "1")
            {
                divColour.Attributes.Add("style", "background-color:Red ");
            }

            else if (Value == "2")
            {
                divColour.Attributes.Add("style", "background-color:green");
            }
            else if (Value == "3")
            {
                divColour.Attributes.Add("style", "background-color:yellow");
            }
          
        }
    }
    
}
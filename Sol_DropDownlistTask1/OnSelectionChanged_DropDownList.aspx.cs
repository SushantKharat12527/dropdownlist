﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_DropDownlistTask1
{
    public partial class OnSelectionChanged_DropDownList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ddlColors_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get Selected Index from DropDown List
            //lblStatus.Text = ddlColors.SelectedIndex.ToString();

            // Get Selected Text from DropDown List
            lblStatus.Text = ddlColors.SelectedItem.Text;

            // Get Selected Value from DropDown List
            //lblStatus.Text = ddlColors.SelectedItem.Value;

            //lblStatus.Text = ddlColors.SelectedValue;
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Task1.aspx.cs" Inherits="Sol_DropDownlistTask1.Task1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:ScriptManager ID="scriptManger" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                
                    <table>
                        <tr>
                            <td>
                                   <asp:TextBox ID="txtAdd" runat="server"></asp:TextBox>
                             </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnAdd" runat="server" Text="Add to List" OnClick="btnAdd_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="ddl" runat="server" ></asp:DropDownList>  
                            </td>
                        </tr>

                    </table>
                    
                   
                
                
                </ContentTemplate>
            </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>

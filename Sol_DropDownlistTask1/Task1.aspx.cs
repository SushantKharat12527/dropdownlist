﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_DropDownlistTask1
{
    public partial class Task1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected   void btnAdd_Click(object sender, EventArgs e)
        {
              this.ColorDropDownListBind();
            

        }

        #region Private Methods
        private async Task ColorDropDownListBind()
        {
            await Task.Run(() => {

                ddl.Items.Add(new ListItem(txtAdd.Text));


            });
        }
        #endregion
    }
}
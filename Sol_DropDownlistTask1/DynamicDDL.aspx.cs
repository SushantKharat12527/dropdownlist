﻿using Sol_DropDownlistTask1.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_DropDownlistTask1
{
    public partial class DynamicDDL : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                await this.BindCities();

                // Set Selected Value in DropDownList
                ddlCity.SelectedValue = "2";
            }

        }

        #region Private Methods
        private async Task BindCities()
        {
            // get Cities Data from Dal
            var citiesObj = await new UserDal().GetCitiesAsync();

            // Bind Data to DropDown List.
            ddlCity.DataSource = citiesObj;
            ddlCity.DataBind();

            // Set Select Text Tittle in Dropdown List
            ddlCity.AppendDataBoundItems = true;
            ddlCity.Items.Insert(0, new ListItem("-- Select City --", "0"));
            //ddlCity.Items.Insert(1, new ListItem("Kishor", "01"));
            ddlCity.SelectedIndex = 0;


        }
        #endregion

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            // Get Selected Index from DropDown List
            //lblStatus.Text = ddlCity.SelectedIndex.ToString();

            // Get Selected Text from DropDown List
            //lblStatus.Text = ddlCity.SelectedItem.Text;

            // Get Selected Value from DropDown List
            //lblStatus.Text = ddlCity.SelectedItem.Value;

            //lblStatus.Text = ddlCity.SelectedValue;


        }
    }
}
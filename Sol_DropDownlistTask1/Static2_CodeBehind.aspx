﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="Static2_CodeBehind.aspx.cs" Inherits="Sol_DropDownlistTask1.Static2_CodeBehind" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:ScriptManager ID="scriptManger" runat="server">

            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>

                    <table>
                        <tr>
                            <td>
                                 <asp:DropDownList ID="ddlColors" runat="server">
                                 </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                                <td>
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                                </td>
                        </tr>
                        <tr>
                                <td>
                                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                </td>
                        </tr>
                    </table>

                   

                </ContentTemplate>
            </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>

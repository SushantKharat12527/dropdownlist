﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_DropDownlistTask1
{
    public partial class Static2_CodeBehind : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                await this.ColorDropDownListBind();
            }
        }

        #region Private Methods
        private async Task ColorDropDownListBind()
        {
            await Task.Run(() => {

                ddlColors.Items.Add(new ListItem("Red", "1"));
                ddlColors.Items.Add(new ListItem("Green", "2"));
                ddlColors.Items.Add(new ListItem("Yellow", "3"));
                

            });
        }
        #endregion

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            // Get Selected Index from DropDown List
            //lblStatus.Text = ddlColors.SelectedIndex.ToString();

            // Get Selected Text from DropDown List
            lblStatus.Text = ddlColors.SelectedItem.Text;

            // Get Selected Value from DropDown List
            //lblStatus.Text = ddlColors.SelectedItem.Value;

            //lblStatus.Text = ddlColors.SelectedValue;
        } 
    }
}